import Foundation

struct AmpelService {
    private let host = "http://192.168.178.40"
    private let redLight = "/manually/red/"
    private let greenLight = "/manually/green/"
    private let on = "on"
    private let off = "off"

    func redLightOn() {
        switchRed(task: on)
    }

    func redLightOff() {
        switchRed(task: off)
    }

    func greeLightOn() {
        switchGreen(task: on)
    }

    func greeLightOff() {
        switchGreen(task: off)
    }

    func allOff() {
        switchRed(task: off)
        switchGreen(task: off)
    }

    private func switchRed(task: String) {
        let url = buildURL(path: redLight, task: task)
        executeRequest(url: url)
    }

    private func switchGreen(task: String) {
        let url = buildURL(path: greenLight, task: task)
        executeRequest(url: url)
    }

    private func buildURL(path: String, task: String) -> URL {
        return URL(string: "\(host)\(path)\(task)")!
    }

    private func executeRequest(url: URL) {
        let task = URLSession.shared.dataTask(with: url) { data, _, _ in
            guard let data = data else { return }
            print(String(data: data, encoding: .utf8)!)
        }

        task.resume()
    }
}
