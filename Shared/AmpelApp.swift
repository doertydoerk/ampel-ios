import SwiftUI

@main
struct AmpelApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
