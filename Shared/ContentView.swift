import SwiftUI

struct ContentView: View {
    let ampel = AmpelService()
    var body: some View {
        VStack {
            Button(action: {
                ampel.redLightOn()
                ampel.greeLightOff()
            }) {
                // Text("red")
                Image("red").resizable().scaledToFit()
            }.padding()

            Button(action: {
                ampel.redLightOff()
                ampel.greeLightOn()
            }) {
                // Text("green")
                Image("green").resizable().scaledToFit()
            }.padding()

            Button(action: {
                ampel.redLightOff()
                ampel.greeLightOff()
            }) {
                Image("switch").resizable().scaledToFit()
            }.padding()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
